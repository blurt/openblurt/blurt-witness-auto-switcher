'use strict';

const blurt = require('@blurtfoundation/blurtjs');

const config = require("config.json")("./config/config.json");

const functions = require('./functions');
const logger = functions.logger;


// Connect to the specified RPC node
const rpc_node = config.rpc_nodes ? config.rpc_nodes[0] : (config.rpc_node ? config.rpc_node : 'https://rpc.blurt.world');
blurt.api.setOptions({uri: rpc_node, url: rpc_node, useAppbase:true });

let missData = [];

startProcess();
setInterval(startProcess, config.interval * 1000);

function getWitness(id) {
    return new Promise((resolve, reject) => {
        blurt.api.getWitnessByAccount(id, function(err, result) {
            if (!err) {
                resolve(result);
            } else {
                reject(err);
            }
        });
    });
}   

function switchTo(signing_key) {
    logger.info(`Switching to ${signing_key}.`);
    const props = {};    
    blurt.broadcast.witnessUpdate(config.key, config.account, config.url, signing_key, props, config.fee, function(err, result) {
        // TODO: You can send a email/SMS here
        if (err) {
            logger.warning(`Failed to broadcast witness update operation!\n${err}`);
        }
        else {
            logger.info(`Successfully broadcasted witness update operation!\n${result}`);
        }
    });    
} 

function reportMissing(missed, totalMissed) {
    logger.warning(`@${config.account} just missed ${missed} blocks. New Total: ${totalMissed}`);
    // TODO: You can send a email/SMS here
}

function startMissingBlocks() {
    return (missData[missData.length - 1] - missData[0]) >= config.threshold;
}

async function startProcess() {
    const account = await getWitness(config.account);
    const signing_key = account.signing_key;
    
    // already disabled, so no point to switch
    if (signing_key === "BLT1111111111111111111111111111111114T1Anm") {
        throw "Witness is disabled; manually broadcast witness update before restarting auto-switcher.";
    }
    
    const total_missed = account.total_missed;
    logger.info(`${signing_key} - total missed: ${total_missed}`);    
    missData.push(total_missed);
    
    // remove outdated entries to avoid memory growing
    if (missData.length > (config.period / config.interval)) {
        missData.shift();
    }
    
    if (missData.length > 2) {
        let missedBlocks = missData[missData.length - 1] - missData[missData.length - 2];
        if (missedBlocks > 0) {
            reportMissing(missedBlocks, total_missed);   
        }
    }

    if (startMissingBlocks()) {   
        // remove current signing key
        const index = config.signing_keys.indexOf(signing_key);
        if (index > - 1) {
            config.signing_keys.splice(index, 1);
        }
        if (config.signing_keys.length === 0) {
            throw "Error: no signing key to use.";
        }     
        switchTo(config.signing_keys[0]);
        // reset data
        missData = [];
    } 
}
