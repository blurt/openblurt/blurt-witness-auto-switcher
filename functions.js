const { createLogger, format, transports } = require('winston');
const { combine, timestamp, colorize, printf } = format;

const logFormat = printf(({ level, message, timestamp }) => {
  return `${timestamp.replace("T", " ").replace("Z", "")} ${level}: ${message}`;
});

module.exports.logger = createLogger({
  level: 'info',
  format: combine(
    colorize({all: true}),
    timestamp(),
    logFormat
  ),
  transports: [new transports.Console()]
});


module.exports.sleep = function(time) {
	return new Promise((resolve) => setTimeout(resolve, time));
}
                          

// check if y array includes any x 
module.exports.arrayInArray = function(x, y) {
  if (!x) { return false; }
  if (!y) { return false; }
  if (!x.constructor === Array) return false;
  if (!y.constructor === Array) return false;
  if (typeof x !== "object") { return false; }
  if (!Array.isArray(x)) { return false; }
  return x.some(r => y.includes(r));
}

module.exports.shuffle = function(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}
