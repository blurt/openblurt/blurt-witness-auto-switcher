# Blurt Witness Auto-Switcher

A program that monitors a Blurt witness for missed blocks, and automatically switches to an alternate server or disables the witness whenever missed blocks go over a certain threshold.

# Installation and Setup

Clone this repo:
```
git clone https://gitlab.com/blurt/openblurt/blurt-witness-auto-switcher.git
```

Navigate to the repo's directory:
```
cd blurt-witness-auto-switcher
```

Make a new directory named `config`:
```
mkdir config
```

Rename `example-config.json` to `config.json`, and copy it to the `config` directory:
```
cp example-config.json config/config.json
```

Edit `config.json` and put in your own info:
```
nano config/config.json
```

Once you have edited `config.json`, press `Ctrl+O`, then `Enter` to save, then press `Ctrl-X` to exit the editor.

If you are going to run the program directly or with pm2, you need to install dependencies before continuing:
```
npm install
```

If you are going to be running the program in a Docker container, there is no need to install dependencies since this will be done automatically during the Docker build stage.


# Running

There are a few options for running the Blurt witness auto-switcher.

## Running Directly

You can run the script directly:
```
npm run production
```

## Running in the Background with pm2

Install pm2:
```
npm install -g pm2
```

Start the script in the background with pm2:

```
pm2 start monitor-witness.js --max-memory-restart 200M
```

Or use the convenient `start.sh` script that is included:
```
./start.sh
```

For more information on pm2 and how to use it, see [this page](https://www.npmjs.com/package/pm2).

## Building and Running in a Docker Container

To build a Docker image and run auto-switcher in a Docker container, do the following after you have completed the installation and setup steps (you should still be in the `blurt-witness-auto-switcher` directory):

Build the Docker image (replace `username` with your own username):
```
docker build -t username/auto-switch .
```

Create a directory called `auto-switcher-config` in your home directory and copy `config.json` to it:
```
mkdir ~/auto-switcher-config && cp config/config.json ~/auto-switcher-config/config.json
```

Navigate to your home directory and start the Docker containter
(replace `username` with the same username used during the build step):
```
cd
docker run -itd --name autoswitch --restart always -v $(pwd)/auto-switcher-config:/app/config username/auto-switch
```

To view the logs while auto-switcher is running:
```
docker logs autoswitch -f
```

To exit the logs, press `Ctrl-C`.

Other useful Docker commands:
```
docker stop autoswitch
docker start autoswitch
socker restart autoswitch
```