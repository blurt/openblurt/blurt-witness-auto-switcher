FROM node:16.13.2-alpine3.15

WORKDIR /app

ADD . /app

RUN npm install

CMD ["npm", "run", "production"]
